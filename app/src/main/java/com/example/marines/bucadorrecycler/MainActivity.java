package com.example.marines.bucadorrecycler;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    ArrayList<Notas> listaNotas;
    RecyclerAdaptador recyclerAdaptador;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Llamamos el recyclerView del xml
        recyclerView = (RecyclerView) findViewById(R.id.rvNotas);
        //iniciar metodo cargarNotas
        cargarNotas();
    }

    public void cargarNotas(){
        //Llamamos el recycler iniciamos y declaramos la orientacion
        recyclerView.setLayoutManager( new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL,false));
      //llamamos el metodo  getlistaNotas de la clase Notas
        listaNotas = new Notas().getlistaNotas();


        //Verificamos que exista un valor en la lista
        if (listaNotas != null) {
            //creamos el adaptador del recycler agregamos la lista de notas y el onIntemClickListener
            recyclerAdaptador = new RecyclerAdaptador(listaNotas, new RecyclerAdaptador.OnItemClickListener() {
                @Override
                //Obtenemos la posicion
                public void onItemClick(final int position) {
                    Toast.makeText(getApplicationContext(),"posiciòn "+position,Toast.LENGTH_LONG).show();
                }
            });
            //Agregamos el adaptador al recycler
            recyclerView.setAdapter(recyclerAdaptador);
        }

    }
    //creamos el buscador


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_buscador,menu);
        MenuItem item = menu.findItem(R.id.buscador);
SearchView search =(SearchView) MenuItemCompat.getActionView(item);


search.setOnQueryTextListener(this);

    MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
            return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        recyclerAdaptador.setFilter(listaNotas);
        return true;
    }
});

        return true;
    }
//se implementan los metodos de busqueda
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

            try {

                ArrayList<Notas> listaFiltrada = filter(listaNotas,newText);
                recyclerAdaptador.setFilter(listaFiltrada);


            }catch (Exception e){
                e.printStackTrace();

            }
        return false;
    }
    //crear metodo para filtrar palabra en la lista
    private ArrayList<Notas> filter(ArrayList<Notas> notas,String texto ){

        ArrayList<Notas> listaFiltrada=new ArrayList<>();
        try{
            texto=texto.toLowerCase();

            for (Notas nota: notas){
                String nota2 =nota.getNota().toLowerCase();

                if (nota2.contains(texto)){
                    listaFiltrada.add(nota);

                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listaFiltrada;
    }
}
